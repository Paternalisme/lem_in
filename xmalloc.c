/*
** xmalloc.c for xmalloc in /home/barbis_j/rendu/Piscine-C-lib/my
** 
** Made by barbis_j
** Login   <barbis_j@epitech.net>
** 
** Started on  Tue Nov  5 16:15:48 2013 barbis_j
** Last update Thu May  1 17:42:57 2014 da-sil_l
*/

#include <stdlib.h>
#include <unistd.h>

void	*xmalloc(int i)
{
  void	*ptr;

  ptr = malloc(i);
  if (ptr == NULL)
    {
      write(1, "memory allocation error\n", 24);
      exit(-1);
    }
  return (ptr);
}
