/*
** init_anthill.c for  in /home/calvez_k/rendu/lem_in
** 
** Made by 
** Login   <calvez_k@epitech.net>
** 
** Started on  Fri May  2 17:01:43 2014 
** Last update Sun May  4 17:13:15 2014 
*/

#include <stdlib.h>
#include "lem_in.h"
#include "graph.h"

void	color_square(t_win *win, int blue, int green, int red)
{
  win->data[win->i++] = blue;
  win->data[win->i++] = green;
  win->data[win->i++] = red;
}

int	my_hill_put_to_img(t_win *win, t_summit *anthill, int x, int y)
{
  win->i = (y * win->sizeline) + x * 4;
  if ((int)win->i > win->sizeline * WINY)
    return (0);
  if (anthill->flag == START)
    color_square(win, 0, 255, 0);
  if (anthill->flag == END)
    color_square(win, 0, 0, 255);
  else
    color_square(win, 255, 255, 255);
  win->data[win->i] = 0;
  return (0);
}

void	draw_hill(t_win *win, t_summit *anthill)
{
  int	x;
  int	y;
  int	fposx;
  int	fposy;

  if (anthill->x <= 0)
    anthill->x = 1;
  if (anthill->y <= 0)
    anthill->y = 1;
  fposx = anthill->x * 20 + 10;
  fposy = anthill->y * 20 + 10;
  x = anthill->x * 20 - 10;
  while (x <= fposx)
    {
      y = anthill->y * 20 - 10;
      while (y <= fposy)
	{
	  my_hill_put_to_img(win, anthill, x, y);
	  ++y;
	}
      ++x;
    }
}

void	fill_coord(t_adj *adj, t_coord *coord, t_win *win)
{
  while (adj != NULL)
    {
      coord->x2 = adj->adj_sum->x * 20;
      if ((coord->y2 = adj->adj_sum->y * 20) <= 0)
	coord->y2 = 20;
      draw_line(win, coord);
      adj = adj->next;
    }
}

void		init_anthill(t_win *win, t_summit *anthill)
{
  t_coord	coord;

  while (anthill != NULL)
    {
      draw_hill(win, anthill);
      coord.x1 = anthill->x * 20;
      if ((coord.y1 = anthill->y * 20) <= 0)
	coord.y1 = 20;
      fill_coord(anthill->adj, &coord, win);
      anthill = anthill->next;
    }
}
