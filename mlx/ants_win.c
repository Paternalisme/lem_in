/*
** ants_win.c for  in /home/calvez_k/rendu/lem_in
** 
** Made by 
** Login   <calvez_k@epitech.net>
** 
** Started on  Sun May  4 16:18:29 2014 
** Last update Sun May  4 17:42:00 2014 da-sil_l
*/

#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include "lem_in.h"
#include "graph.h"

void		my_path_to_img(t_summit *sum, int x, int y, t_win *win)
{
  int		i;

  i = win->i;
  win->i = y * win->sizeline + x * 4;
  if ((int)win->i > win->sizeline * WINY)
    return ;
  if (sum->ants == 1 && sum->flag == 0)
    {
      win->data[win->i++] = 90 * i;
      win->data[win->i++] = 42 * i;
      win->data[win->i++] = 60 * i;
    }
  else if (sum->flag == 0)
    {
      win->data[win->i++] = 255;
      win->data[win->i++] = 255;
      win->data[win->i++] = 255;
    }
  win->data[win->i] = 0;
}

void	draw_path(t_win *win, t_adj *path, int i)
{
  int	x;
  int	y;
  int	fposx;
  int	fposy;

  if (path->adj_sum->x <= 0)
    path->adj_sum->x = 1;
  if (path->adj_sum->y <= 0)
    path->adj_sum->y = 1;
  fposx = path->adj_sum->x * 20 + 7;
  fposy = path->adj_sum->y * 20 + 7;
  x = path->adj_sum->x * 20 - 7;
  while (x <= fposx)
    {
      y = path->adj_sum->y * 20 - 7;
      while (y <= fposy)
	{
	  win->i = i;
	  my_path_to_img(path->adj_sum, x, y, win);
	  ++y;
	}
      ++x;
    }
}

void		move_ants_win(t_adj *path, t_win *win)
{
  int		max_ants;
  t_adj		*end;
  int		i;

  max_ants = path->adj_sum->ants;
  end = end_path(path);
  while (end->adj_sum->ants != max_ants)
    {
      path = end;
      i = path->adj_sum->ants + 1;
      while (path->prev != NULL)
	{
	  if (path->prev->adj_sum->ants > 0)
	    {
	      path->adj_sum->ants = path->adj_sum->ants + 1;
	      path->prev->adj_sum->ants = path->prev->adj_sum->ants - 1;
	      printf("P%d-%s ", i++, path->adj_sum->sid);
	    }
	  draw_path(win, path, i);
	  path = path->prev;
	}
      gere_expose(win);
      sleep(1);
      printf("\n");
    }
}
