/*
** draw_anthill.c for  in /home/calvez_k/rendu/lem_in
** 
** Made by 
** Login   <calvez_k@epitech.net>
** 
** Started on  Fri May  2 16:20:01 2014 
** Last update Sun May  4 16:34:52 2014 
*/

#include <mlx.h>
#include <stdlib.h>
#include "lem_in.h"
#include "graph.h"

int		gere_key(int key, t_win *win)
{
  if (key == EXIT)
    exit(1);
  return (0);
}

int		gere_expose(t_win *win)
{
  mlx_put_image_to_window(win->mlx_ptr,
                          win->win_ptr,
                          win->img_ptr, 0, 0);
  return (0);
}

int		new_img(t_win *win)
{
  if ((win->mlx_ptr = mlx_init()) == 0)
    return (0);
  if ((win->win_ptr =
       mlx_new_window(win->mlx_ptr, WINX, WINY, "Anthill")) == 0)
    return (0);
  if ((win->img_ptr =
       mlx_new_image(win->mlx_ptr, WINX, WINY)) == NULL)
    return (0);
  if ((win->data = mlx_get_data_addr(win->img_ptr,
				     &win->bpp,
				     &win->sizeline,
				     &win->endian)) == NULL)
    return (0);
  return (1);
}

int	draw_anthill(t_summit *anthill, t_win *win, t_adj *path)
{
  int	ret;

  if ((ret = new_img(win)) == 0)
    return (0);
  init_anthill(win, anthill);
  move_ants_win(path, win);
  mlx_hook(win->win_ptr, 2, 1, gere_key, win);
  mlx_expose_hook(win->win_ptr, gere_expose, win);
  mlx_loop(win->mlx_ptr);
  return (0);
}
