/*
** graph.h for  in /home/calvez_k/rendu/lem_in
** 
** Made by 
** Login   <calvez_k@epitech.net>
** 
** Started on  Fri May  2 17:07:18 2014 
** Last update Sun May  4 17:41:02 2014 da-sil_l
*/

#ifndef GRAPH_H_
# define GRAPH_H_

# define EXIT	65307
# define WINX	800
# define WINY	600

#include "lem_in.h"

typedef struct		s_win
{
  void			*mlx_ptr;
  void			*win_ptr;
  void			*img_ptr;
  int			sizeline;
  char			*data;
  void			*img;
  int			bpp;
  int			endian;
  unsigned long		i;
}			t_win;

typedef struct		s_coord
{
  int			x1;
  int			y1;
  int			x2;
  int			y2;
}			t_coord;

void		move_ants_win(t_adj *, t_win *);
void		init_anthill(t_win *, t_summit *);
void		draw_line(t_win *, t_coord *);
void		drawline(t_coord *, t_win *);
void		drawlinex(t_coord *, t_win *);
void		drawliney(t_coord *, t_win *);
int		gere_expose(t_win *);
int		draw_anthill(t_summit *, t_win *, t_adj *);

#endif /* !GRAPH_H_ */
