/*
** aff_ligne.c for  in /home/calvez_k/rendu/lem_in
** 
** Made by 
** Login   <calvez_k@epitech.net>
** 
** Started on  Sat May  3 18:42:10 2014 
** Last update Sun May  4 16:28:15 2014 
*/

#include "graph.h"

void		mlx_put_img(int x, int y, t_win *win)
{
  win->i = y * win->sizeline + x * 4;
  if ((int)win->i > win->sizeline * WINY)
    return ;
  win->data[win->i++] = 0;
  win->data[win->i++] = 255;
  win->data[win->i++] = 255;
  win->data[win->i] = 0;
}

void		swap_coord(t_coord *coord)
{
  int		aux;

  aux = coord->x1;
  coord->x1 = coord->x2;
  coord->x2 = aux;
  aux = coord->y1;
  coord->y1 = coord->y2;
  coord->y2 = aux;
}

void		draw_line(t_win *win, t_coord *coord)
{
  double	a;

  a = (double)(coord->y2 - coord->y1) / (coord->x2 - coord->x1);
  if (a > 1 || a < -1)
    {
      if (coord->y1 <= coord->y2)
	swap_coord(coord);
      drawliney(coord, win);
    }
  else
    {
      if (coord->x1 >= coord->x2)
	swap_coord(coord);
      drawlinex(coord, win);
    }
}

void		drawlinex(t_coord *coord, t_win *win)
{
  int		x;

  x = coord->x1;
  while (x <= coord->x2)
    {
      if ((coord->x2 - coord->x1) != 0)
	mlx_put_img(x, coord->y1 +
		    ((coord->y2 - coord->y1) * (x - coord->x1)) /
		    (coord->x2 - coord->x1), win);
      ++x;
    }
}

void		drawliney(t_coord *coord, t_win *win)
{
  int		y;

  y = coord->y1;
  while (y >= coord->y2)
    {
      if ((coord->y2 - coord->y1) != 0)
      mlx_put_img(coord->x1
		  + ((coord->x2 - coord->x1) * (y - coord->y1))
		  / (coord->y2 - coord->y1), y, win);
      --y;
    }
}
