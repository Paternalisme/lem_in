/*
** algo.c for lemin in /home/barbis_j/Documents/Projets/prog_elem/lem_in
** 
** Made by barbis_j
** Login   <barbis_j@epitech.net>
** 
** Started on  Mon Apr 28 17:14:33 2014 barbis_j
** Last update Fri May  2 20:37:36 2014 da-sil_l
*/

#include <stdlib.h>
#include "lem_in.h"

t_summit	*find_start(t_summit *anthill)
{
  while (anthill != NULL && anthill->flag != 1)
    anthill = anthill->next;
  return (anthill);
}

t_summit	*find_min(t_summit *anthill)
{
  t_summit	*min;
  t_summit	*tmp;

  tmp = anthill;
  while (tmp != NULL && tmp->browsed == 1)
    tmp = tmp->next;
  min = tmp;
  while (anthill != NULL)
    {
      if (anthill->browsed != 1
	  && anthill->dist < min->dist)
	min = anthill;
      anthill = anthill->next;
    }
  return (min);
}

int		check_end(t_summit *anthill)
{
  while (anthill != NULL)
    {
      if (anthill->flag == END && anthill->browsed == 1)
	return (0);
      anthill = anthill->next;
    }
  return (1);
}

t_summit	*find_path(t_summit *anthill)
{
  t_summit	*s1;
  t_adj		*adj_save;

  while (check_end(anthill))
    {
      s1 = find_min(anthill);
      s1->browsed = 1;
      adj_save = s1->adj;
      while (s1->adj != NULL)
	{
	  if (s1->adj->adj_sum->browsed != 1 &&
	      s1->dist + 1 < s1->adj->adj_sum->dist)
	    s1->adj->adj_sum->dist = s1->dist + 1;
	  s1->adj = s1->adj->next;
	}
      s1->adj = adj_save;
    }
  return (anthill);
}

int		algo(t_summit *anthill)
{
  anthill = find_path(anthill);
  return (0);
}
