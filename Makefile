#
## Makefile for 42sh in /home/da-sil_l/Dropbox/ALL/Dropbox/Clement/test
## 
## Made by da-sil_l
## Login   <da-sil_l@epitech.net>
## 
## Started on  Wed Mar 19 13:31:30 2014 da-sil_l
## Last update Sun May  4 17:44:57 2014 da-sil_l
##

CC		= gcc -g

RM		= rm -f

NAME		= lem_in

SRCS		= main.c 		\
		  fill_list.c 		\
		  fill_adj.c		\
		  utils.c		\
		  ants.c		\
		  is_it.c		\
		  get_next_line.c 	\
		  parsing.c 		\
		  wordtab.c 		\
		  xmalloc.c 		\
		  algo.c 		\
		  make_path.c		\
		  my_getnbr.c		\
		  mlx/ants_win.c	\
		  mlx/draw_anthill.c	\
		  mlx/init_anthill.c	\
		  mlx/aff_ligne.c

OBJS		= $(SRCS:.c=.o)

CFLAGS		+= -I. -Wall -Wextra \
		   -Wno-unused-variable -Wno-unused-parameter \
		   -Wno-parentheses

LIBX	= -L/usr/lib64 -lmlx_$$HOSTTYPE -L/usr/lib64/X11 -lXext -lX11 -lm


all: $(NAME)

$(NAME): $(OBJS)
	$(CC) -o $(NAME) $(OBJS) $(LIBX)

clean:
	$(RM) $(OBJS)

fclean: clean
	$(RM) $(NAME)

re: fclean all

.PHONY: all clean fclean re
