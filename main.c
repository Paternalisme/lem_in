/*
** main.c for lemin in /home/barbis_j/Documents/Projets/prog_elem/lem_in
** 
** Made by barbis_j
** Login   <barbis_j@epitech.net>
** 
** Started on  Thu Apr 24 15:09:07 2014 barbis_j
** Last update Sun May  4 16:16:53 2014 
*/

#include <stdlib.h>
#include <stdio.h>
#include "lem_in.h"
#include "graph.h"

void		show_list(t_summit *anthill)
{
  t_adj		*save;

  while (anthill != NULL)
    {
      printf("sid %s : dist %d, browsed %d, flag %d, ants %d, x: %d, y: %d\n",
	     anthill->sid, anthill->dist, anthill->browsed,
	     anthill->flag, anthill->ants, anthill->x, anthill->y);
      save = anthill->adj;
      while (anthill->adj != NULL)
	{
	  printf("BRANCHE : sid %s, dist %d, browsed %d\n",
		 anthill->adj->adj_sum->sid,
		 anthill->adj->adj_sum->dist, anthill->adj->adj_sum->browsed);
	  anthill->adj = anthill->adj->next;
	}
      anthill->adj = save;
      anthill = anthill->next;
    }
}

int		main(int ac, char **av)
{
  t_summit	*anthill;
  t_win		win;
  char		***file;
  t_adj		*path;

  file = pars();
  anthill = NULL;
  if ((anthill = fill_list(file, anthill)) == NULL)
    return (-1);
  algo(anthill);
  path = make_path(anthill);
  if (ac == 2 && !my_strcmp(av[1], "-mlx"))
    draw_anthill(anthill, &win, path);
  move_ants(path);
  return (0);
}
