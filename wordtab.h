/*
** wordtab.h for wordtab in /home/barbis_j/Documents/Projets/wordtab
** 
** Made by barbis_j
** Login   <barbis_j@epitech.net>
** 
** Started on  Mon Dec  9 14:10:23 2013 barbis_j
** Last update Tue Dec 10 13:59:04 2013 barbis_j
*/

#ifndef WORDTAB_H_
# define WORDTAB_H_

# define COND(c) ((c) == ' ' ? 1 : 0)

#endif /* !WORDTAB_H_ */
