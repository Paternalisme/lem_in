/*
** fill_list.c for lemin in /home/barbis_j/Documents/Projets/prog_elem/lem_in
** 
** Made by barbis_j
** Login   <barbis_j@epitech.net>
** 
** Started on  Fri Apr 25 17:07:42 2014 barbis_j
** Last update Sun May  4 16:48:03 2014 
*/

#include <stdlib.h>
#include <unistd.h>
#include "lem_in.h"

void	add_dist(t_summit *anthill)
{
  while (anthill != NULL)
    {
      if (anthill->flag == START)
	anthill->dist = 0;
      else
	anthill->dist = INFINITY;
      anthill = anthill->next;
    }
}

t_summit	*add_summit(t_summit *anthill, char **info, int flag, char *ants)
{
  t_summit	*summit;

  summit = xmalloc(sizeof(t_summit));
  summit->ants = my_getnbr(ants);
  summit->browsed = 0;
  summit->sid = info[0];
  summit->x = 1;
  summit->y = 1;
  if (info[1])
    summit->x = my_getnbr(info[1]);
  if (info[2])
    summit->y = my_getnbr(info[2]);
  summit->flag = flag;
  summit->adj = NULL;
  summit->next = anthill;
  anthill = summit;
  return (anthill);
}

t_summit	*fill_summit(char ***file, t_summit *anthill)
{
  int		i;

  i = 0;
  while (file[i] && !is_arc(file[i][0]))
    {
      if (is_it(file[i], anthill, "##start") && file[i + 1])
	anthill = add_summit(anthill, file[++i], START, file[0][0]);
      else if (is_it(file[i], anthill, "##end") && file[i + 1])
	anthill = add_summit(anthill, file[++i], END, "0");
      else if (file[i] && file[i][0][0] != '#' && file[i][1])
	anthill = add_summit(anthill, file[i], 0, "0");
      ++i;
    }
  add_dist(anthill);
  list_verify(anthill);
  return (anthill);
}

t_summit	*fill_list(char ***file, t_summit *anthill)
{
  if ((anthill = fill_summit(file, anthill)) == NULL)
    return (NULL);
  fill_adj(file, anthill);
  return (anthill);
}

void		list_verify(t_summit *anthill)
{
  int		start;
  int		end;

  start = 0;
  end = 0;
  while (anthill != NULL)
    {
      if (anthill->flag == START)
	++start;
      else if (anthill->flag == END)
	++end;
      anthill = anthill->next;
    }
  if (start != 1 || end != 1)
    {
      write(2, ERR_ANT, my_strlen(ERR_ANT));
      exit(-1);
    }
}
