/*
** make_path.c for lemin in /home/barbis_j/Documents/Projets/prog_elem/lem_in
** 
** Made by barbis_j
** Login   <barbis_j@epitech.net>
** 
** Started on  Fri May  2 15:58:53 2014 barbis_j
** Last update Sat May  3 18:29:27 2014 da-sil_l
*/

#include <stdlib.h>
#include <unistd.h>
#include "lem_in.h"

void	error_path()
{
  write(2, ERR_PATH, my_strlen(ERR_PATH));
  exit(-1);
}

int	box_nbr(t_summit *anthill)
{
  int	box;

  box = 0;
  while (anthill)
    {
      anthill = anthill->next;
      ++box;
    }
  return (box);
}

t_summit	*find_end(t_summit *anthill)
{
  while (anthill != NULL)
    {
      if (anthill->flag == END)
	return (anthill);
      anthill = anthill->next;
    }
  return (NULL);
}

t_adj		*make_path(t_summit *anthill)
{
  t_adj		*path;
  int		max;
  int		turn;

  turn = 0;
  max = box_nbr(anthill) + 2;
  path = NULL;
  anthill = find_end(anthill);
  while (anthill->dist != 0 && turn < max)
    {
      while (anthill->adj &&
	     anthill->adj->adj_sum->dist != anthill->dist - 1)
	  anthill->adj = anthill->adj->next;
      path = add_adj(anthill, path);
      if (anthill->adj)
	anthill = anthill->adj->adj_sum;
      ++turn;
    }
  if (turn == max)
    error_path();
  else
    path = add_adj(anthill, path);
  return (path);
}
