/*
** lem_in.h for lemin in /home/barbis_j/Documents/Projets/prog_elem/lem_in
** 
** Made by barbis_j
** Login   <barbis_j@epitech.net>
** 
** Started on  Wed Apr 23 17:25:17 2014 barbis_j
** Last update Sun May  4 17:40:26 2014 da-sil_l
*/

#ifndef LEM_IN_H_
# define LEM_IN_H_

# define ERR_ANT "Invalid anthill\n"
# define ERR_PATH "No way out, ants are going to die\n"
# define START 1
# define END 2
# define INFINITY 9999

struct			s_summit;

typedef struct		s_adj
{
  struct s_summit	*adj_sum;
  struct s_adj		*prev;
  struct s_adj		*next;
}			t_adj;

typedef struct		s_summit
{
  int			browsed;
  int			dist;
  int			flag;
  int			ants;
  int			x;
  int			y;
  char			*sid;
  struct s_summit	*next;
  struct s_adj		*adj;
}			t_summit;

int		sizetab(char ***);
int		is_arc(char *);
int		is_it(char **, t_summit *, char *);
int		algo(t_summit *);
int		my_strcmp(char *, char *);
int		my_strlen(char *);
int		my_strncpy(char *, char *, int);
int		my_getnbr(char *);
int		init_end(char *, int, int *);
char		*get_next_line(const int);
char		**wordtab(char *);
char		***pars();
char		***tab_realloc(char ***);
void		show_list(t_summit *);
void		tab_cpy_char(char ***, char ***);
void		put_adj(char *, char *, t_summit *);
void		move_ants(t_adj *);
void		fill_adj(char ***, t_summit *);
void		list_verify(t_summit *);
void		*xmalloc(int);
t_adj		*add_adj(t_summit *, t_adj *);
t_adj		*make_path(t_summit *);
t_adj		*end_path(t_adj *);
t_summit	*find_end(t_summit *);
t_summit	*find_path(t_summit *);
t_summit	*fill_list(char ***, t_summit *);
t_summit	*add_summit(t_summit *, char **, int, char *);
t_summit	*fill_summit(char ***, t_summit *);
t_summit	*find_start(t_summit *);
t_summit	*find_min(t_summit *);

#endif /* !LEM_IN_H_ */
