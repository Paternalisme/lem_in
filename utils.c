/*
** utils.c for zboub in /home/da-sil_l/BitBucket/lem_in
** 
** Made by da-sil_l
** Login   <da-sil_l@epitech.net>
** 
** Started on  Fri May  2 19:14:08 2014 da-sil_l
** Last update Fri May  2 19:14:49 2014 da-sil_l
*/

void		my_strncpy(char *dest, char *src, int n)
{
  int		i;

  i = 0;
  while (i < n)
    {
      dest[i] = src[i];
      i = i + 1;
    }
  dest[i] = 0;
}

int	my_strcmp(char *s1, char *s2)
{
  int	i;

  i = 0;
  while (s1[i] && s2[i] && s1[i] == s2[i])
    ++i;
  return (s1[i] - s2[i]);
}

int	my_strlen(char *str)
{
  int	i;

  i = 0;
  while (str[i])
    ++i;
  return (i);
}
