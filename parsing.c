/*
** parsing.c for lemin in /home/barbis_j/Documents/Projets/prog_elem/lem_in
** 
** Made by barbis_j
** Login   <barbis_j@epitech.net>
** 
** Started on  Thu Apr 24 15:10:46 2014 barbis_j
** Last update Sat May  3 20:22:28 2014 da-sil_l
*/

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include "lem_in.h"

void   	print_file(char ***file)
{
  int	i;
  int	j;

  i = 0;
  while (file[i])
    {
      j = 0;
      while (file[i][j])
	{
	  write(1, file[i][j], my_strlen(file[i][j]));
	  if (file[i][j + 1])
	    write(1, " ", 1);
	  ++j;
	}
      ++i;
      write(1, "\n", 1);
    }
}

int		sizetab(char ***tab)
{
  int		i;

  i = 0;
  while (tab[i] != NULL)
    ++i;
  return (i);
}

void		tab_cpy(char ***dest, char ***src)
{
  int		i;

  i = 0;
  while (src[i] != NULL)
    {
      dest[i] = src[i];
      ++i;
    }
  dest[i] = NULL;
}

char		***tab_realloc(char ***tab)
{
  char		***tmp;

  tmp = xmalloc((sizetab(tab) + 2) * sizeof(char **));
  tab_cpy(tmp, tab);
  free(tab);
  return (tmp);
}

char		***pars()
{
  char		*line;
  char		**tab_line;
  char		***file;
  int		i;

  i = 0;
  file = xmalloc(2 * sizeof(char **));
  while ((line = get_next_line(0)) != NULL)
    {
      if (line[0] != '\0' && (tab_line = wordtab(line)) != NULL)
	{
	  file[i] = tab_line;
	  file[i + 1] = NULL;
	  file = tab_realloc(file);
	  ++i;
	}
      free(line);
    }
  print_file(file);
  return (file);
}
