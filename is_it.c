/*
** is_it.c for chtromol in /home/da-sil_l/BitBucket/lem_in
** 
** Made by da-sil_l
** Login   <da-sil_l@epitech.net>
** 
** Started on  Fri May  2 16:06:45 2014 da-sil_l
** Last update Sun May  4 17:54:24 2014 da-sil_l
*/

#include <stdlib.h>
#include <unistd.h>
#include "lem_in.h"

int	is_arc(char *str)
{
  int	i;

  i = 0;
  if (!str)
    {
      write(2, "Error syntax\n", 13);
      exit(-1);
    }
  while (str[i])
    {
      if (str[i] == '-')
	return (1);
      ++i;
    }
  return (0);
}

int	is_it(char **file, t_summit *anthill, char *pattern)
{
  if (!my_strcmp(file[0], pattern))
    return (1);
  return (0);
}
