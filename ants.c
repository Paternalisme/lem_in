/*
** ants.c for lemin in /home/barbis_j/Documents/Projets/prog_elem/lem_in
** 
** Made by barbis_j
** Login   <barbis_j@epitech.net>
** 
** Started on  Sat May  3 15:47:29 2014 barbis_j
** Last update Sun May  4 16:19:07 2014 
*/

#include <stdlib.h>
#include <stdio.h>
#include "lem_in.h"
#include "graph.h"

t_adj		*end_path(t_adj *path)
{
  while (path->next != NULL)
    path = path->next;
  return (path);
}

void		move_ants(t_adj *path)
{
  int		max_ants;
  t_adj		*end;
  int		i;

  max_ants = path->adj_sum->ants;
  end = end_path(path);
  while (end->adj_sum->ants != max_ants)
    {
      path = end;
      i = path->adj_sum->ants + 1;
      while (path->prev != NULL)
	{
	  if (path->prev->adj_sum->ants > 0)
	    {
	      path->adj_sum->ants = path->adj_sum->ants + 1;
	      path->prev->adj_sum->ants = path->prev->adj_sum->ants - 1;
	      printf("P%d-%s ", i, path->adj_sum->sid);
	      ++i;
	    }
	  path = path->prev;
	}
      printf("\n");
    }
}
