#!/usr/local/bin/perl -w
## gmin.pl for  lemin in /home/barbis_j/Documents/Projets/prog_elem/lem_in
## 
## Made by barbis_j
## Login   <barbis_j@epitech.net>
## 
## Started on  Tue Apr 22 16:21:50 2014 barbis_j
## Last update Thu May  1 18:50:31 2014 da-sil_l
##

use strict;

# gmin.pl taille densite nb home name end name

if (scalar @ARGV < 3)
{
    print "usage : gmin.pl size density nb_of_fourmiz\nWhen size is the number of rooms and density the percentage of probability of connexions between rooms\n";
    exit -1;
}

my $i = 0;
my $j = 0;

my $size = shift @ARGV;
my $density = int(shift @ARGV);
my $nb = int(shift @ARGV);

my $home = int(rand($size));
my $end  = int(rand($size));
while ($end == $home)
{
    my $end  = int(rand($size));
}

print $nb . "\n";
while ($i < $size)
{
    print "##start\n" if ($i == $home);
    print "##end\n" if ($i == $end);
    print $i;
    print " " . int(rand(10 * $size));
    print " " . int(rand(10 * $size));
    print "\n";
    $i++;
}

$i = 0;

while ($i < $size)
{
    $j = 0;
    while ($j < $size)
    {
	if ($density > int(rand(100)))
	{
	    print $i . "-" . $j . "\n";
	}
	$j++;
    }
    $i++;
}
