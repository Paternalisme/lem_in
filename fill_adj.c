/*
** fill_adj.c for plotron in /home/da-sil_l/BitBucket/lem_in
** 
** Made by da-sil_l
** Login   <da-sil_l@epitech.net>
** 
** Started on  Fri May  2 19:09:13 2014 da-sil_l
** Last update Sat May  3 20:40:16 2014 da-sil_l
*/

#include <stdlib.h>
#include "lem_in.h"

void		fill_adj(char ***file, t_summit *anthill)
{
  int		i;
  int		j;

  i = 0;
  while (file[i] != NULL)
    {
      j = 0;
      while (file[i][0][j] != 0 && file[i][0][j] != '-')
	++j;
      if (file[i][0][j] && file[i][0][j + 1])
	{
	  file[i][0][j] = 0;
	  put_adj(file[i][0], &file[i][0][++j], anthill);
	}
      put_adj(&file[i][0][j], file[i][0], anthill);
      ++i;
    }
}

void		put_adj(char *summit, char *adj, t_summit *anthill)
{
  t_summit	*adj_summit;

  adj_summit = anthill;
  while (anthill != NULL && my_strcmp(summit, anthill->sid) != 0)
    anthill = anthill->next;
  if (anthill == NULL)
    return ;
  while (adj_summit != NULL && my_strcmp(adj, adj_summit->sid) != 0)
    adj_summit = adj_summit->next;
  if (adj_summit == NULL)
    return ;
  anthill->adj = add_adj(adj_summit, anthill->adj);
}

t_adj		*add_adj(t_summit *adj_sum, t_adj *adj)
{
  t_adj		*new_adj;

  new_adj = xmalloc(sizeof(t_adj));
  new_adj->adj_sum = adj_sum;
  new_adj->next = adj;
  new_adj->prev = NULL;
  if (adj != NULL)
    adj->prev = new_adj;
  adj = new_adj;
  return (adj);
}
